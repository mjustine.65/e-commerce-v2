import { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import {Card, Table, Modal, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductCard({prop}) {
  
  // getting global state/value of id and isAdmin
  const { idGlobal, isAdminGlobal } = useContext(UserContext);

  // init states/values
  const navigate = useNavigate()
  const [isOpenDetailsModal, setIsOpenDetailsModal] = useState(() => false);
  const [isOpenToCartModal, setIsOpenToCartModal] = useState(() => false);
  const [qty, setQty] = useState(() => 1);


  // const [newBrand, setNewBrand] = useState(brand);
  // const [newStyle, setNewStyle] = useState(style);
  // const [newModel, setNewModel] = useState(model);
  // const [newSize, setNewSize] = useState(size);
  // const [newColor, setNewColor] = useState(color);
  // const [newSku, setNewSku] = useState(sku);
  // const [newPrice, setNewPrice] = useState(price);
  // const [newStocks, setNewStocks] = useState(stocks);
  // const [newIsActive, setNewIsActive] = useState(isActive);
  // const [isOpen2, setIsOpen2] = useState(false);

  const viewDetailsModal = () => {

    setIsOpenDetailsModal(prevValue => true);
    return;
  };

  const hideDetailsModal = () => {

    setIsOpenDetailsModal(prevValue => false);
    return; 
  };

  const addToCartModal = () => {

    setIsOpenToCartModal(prevValue => true);
    return;
  };

  const hideAddToCartModal = () => {

    setIsOpenToCartModal(prevValue => false );
    return;
  };

  const handleAddToCart = (e) => {

    e.preventDefault();

    // return fetch(`${process.env.REACT_APP_API_URL}products/add-to-cart`, {
    return fetch(`${process.env.REACT_APP_API_URL}products/to-cart`, {
      
      method: 'POST',
      headers: {
        'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        newProdId: prop._id,
        newQty: qty,
      })
    })

    .then(res => res.json())
    .then(data => {

      if (data === true) {

        hideAddToCartModal();

        Swal.fire({
          title: 'Added To Cart Successfully!',
          icon: 'success',
        });
        return;
      };
    })
  }

  // const showModal2 = () => {

  //   return setIsOpen2(true);
  // };

  // const hideModal2 = () => {

  //   return setIsOpen2(false);
  // };

  // const handleEditProduct = (e) => {

  //   e.preventDefault();

  //   // return fetch(`${process.env.REACT_APP_API_URL}products/edit`, {
  //   return fetch('http://localhost:4000/products/edit', {

  //     method: 'PUT',
      
  //     headers: {
  //       'Content-Type':'application/json',
  //       Authorization: `Bearer ${localStorage.getItem('token')}`
  //     },

  //     body: JSON.stringify({
  //       id: _id,
  //       brand: newBrand,
  //       style: newStyle,
  //       model: newModel,
  //       size: newSize,
  //       color: newColor,
  //       sku: newSku,
  //       price: newPrice,
  //       stocks: newStocks,
  //       isActive: newIsActive
  //     })
  //   },)
  //   .then(res => res.json())
  //   .then(data => {

  //     if (data.result === true) {

  //       hideModal();
        
  //       window.location.reload(false)

  //       // return Swal.fire({
  //       //   title: 'Successfully Save Changes!',
  //       //   icon: 'success',
  //       // });
  //     };

  //   });
  // };

  

  const redirectToLogin = () => {

    navigate("/users/login")
    return;
  }
	return (
    (idGlobal === null || isAdminGlobal === false)
    ? <>
      <div className="col-lg-4 mb-4">
        <Card className="container">
          <Card.Body>
            <Card.Title>{prop.brand}</Card.Title>
            <Card.Subtitle className="mb-3">{`${prop.model} | ${prop.color}`}</Card.Subtitle>
            <Card.Text> Price: PHP {prop.price}.00</Card.Text>
            <button type="button" className="btn btn-outline-danger rounded-pill w-100 mb-1" onClick={viewDetailsModal}>View Details</button>
            {/* conditional redering for add to cart btn */}
            {
              (isAdminGlobal === false) ?
              <button type="button" className="btn btn-outline-danger rounded-pill w-100" onClick={addToCartModal}>Add To Cart</button>
              : <button type="button" className="btn btn-outline-danger rounded-pill w-100" onClick={redirectToLogin}>Login To Shop</button>
            }
            {/* end of conditional rendering */}
          </Card.Body>
        </Card>

        {/* MODAL FOR VIEWING DETAILS */}
        <Modal show={isOpenDetailsModal} onHide={hideDetailsModal}>
          <Modal.Body>
            <table className="table table-striped table-bordered">
              <tbody >
                <tr>
                  <td>Brand:</td>
                  <td>{prop.brand}</td>
                </tr>
                <tr>
                  <td>Model:</td>
                  <td>{prop.model}</td>
                </tr>
                <tr>
                  <td>Style:</td>
                  <td>{prop.style}</td>
                </tr>
                <tr>
                  <td>Color:</td>
                  <td>{prop.color}</td>
                </tr>
                <tr>
                  <td>SKU:</td>
                  <td>{prop.sku}</td>
                </tr>
                <tr>
                  <td>Price:</td>
                  <td>PHP {prop.price}.00</td>
                </tr>
              </tbody>
            </table>
          </Modal.Body>
          <Modal.Footer>
              <Button variant="outline-danger rounded-pill" onClick={hideDetailsModal}>Close</Button>
          </Modal.Footer>
        </Modal>

        {/* MODAL FOR ADDING PRODUCT TO CART */}
        <Modal show={isOpenToCartModal} onHide={hideAddToCartModal}>
          <Form>
          <Modal.Body>
            <table className="table table-striped table-bordered">
              <tbody >
                <tr>
                  <td>SKU:</td>
                  <td>
                    <Form.Control
                        className="mb-2" 
                        type="text"
                        value={prop.sku} 
                        readOnly 
                        disabled />
                  </td>
                </tr>
                <tr>
                  <td>Qty:</td>
                  <td>
                  <Form.Control
                      className="mb-2" 
                      type="number"
                      value={qty} 
                      onChange={e => setQty(preValue => e.target.value)}/></td>
                </tr>
              </tbody>
            </table>
          </Modal.Body>
          <Modal.Footer>
              <Button variant="outline-danger rounded-pill" onClick={handleAddToCart}>Add</Button>
          </Modal.Footer>
          </Form>
        </Modal>
      </div>
    </>
    : <></>/*(isAdminGlobal)
      ? <>
        <section className="container overflow-auto">
          <div className="row justify-content-center">
            <div className="col-lg-10">
              <Card className="container">
                <Card.Body>
                  <Card.Title>{`Brand: ${brand}`}</Card.Title>
                  <Card.Subtitle className="mb-3">{`Product ID: ${_id}`}</Card.Subtitle>
                    <Table striped >
                      <tbody >
                        <tr>
                          <td>Style:</td>
                          <td>{style}</td>
                        </tr>
                        <tr>
                          <td>Model:</td>
                          <td>{model}</td>
                        </tr>
                        <tr>
                          <td>Size And Color:</td>
                          <td>{`${size} | ${color}`}</td>
                        </tr>
                        <tr>
                          <td>SKU:</td>
                          <td>{sku}</td>
                        </tr>
                        <tr>
                          <td>Quantity and Price:</td>
                          <td>{`${stocks} | ${price}`}</td>
                        </tr>
                        <tr>
                          <td>Active:</td>
                          <td>{(isActive) ? 'Yes' : 'No'}</td>
                        </tr>
                      </tbody>
                    </Table>
                  <button type="button" className="btn btn-outline-danger rounded-pill" onClick={showModal}>Edit</button>
                  <Modal show={isOpen} onHide={hideModal}>
                    <Form onSubmit={e => handleEditProduct(e)}>
                    <Modal.Header>
                      <Modal.Title><h4>Edit Product</h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form.Group>
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={newBrand}
                              onChange={e => setNewBrand(e.target.value)}
                              placeholder="* Brand (e.g. SMK, AVG, LS2)"/>
                          <Form.Select aria-label="Default select example" className="mb-2" value={newStyle} onChange={e => setNewStyle(e.target.value)}>
                              <option>Select Style</option>
                              <option value="Full-face">Full Face</option>
                              <option value="Half-face">Half Face</option>
                              <option value="Modular">Modular</option>
                              <option value="Off road">Off Road</option>
                          </Form.Select>
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={newModel}
                              onChange={e => setNewModel(e.target.value)}
                              placeholder="* Model Name" />
                          <Form.Select aria-label="Default select example" className="mb-2" value={newSize} onChange={e => setNewSize(e.target.value)}>
                              <option>Select Size</option>
                              <option value="XS">Extra Small</option>
                              <option value="SM">Small</option>
                              <option value="MD">Medium</option>
                              <option value="LG">Large</option>
                              <option value="XL">Extra Large</option>
                          </Form.Select>
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={newColor}
                              onChange={e => setNewColor(e.target.value)}
                              placeholder="* Color" />
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={newSku}
                              onChange={e => setNewSku(e.target.value)}
                              placeholder="* SKU (e.g. EXHE-STEXS000)" />
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={newPrice}
                              onChange={e => setNewPrice(e.target.value)}
                              placeholder="* Price" />
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={newStocks}
                              onChange={e => setNewStocks(e.target.value)}
                              placeholder="* Stocks" />
                          <Form.Select aria-label="Default select example" className="mb-2" value={newIsActive} onChange={e => setNewIsActive(e.target.value)}>
                              <option>Yes/No</option>
                              <option value="true">Yes</option>
                              <option value="false">No</option>
                          </Form.Select>
                      </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="outline-danger rounded-pill" type="submit">Save</Button>
                        <Button variant="outline-danger rounded-pill" onClick={hideModal}>Cancel</Button>
                    </Modal.Footer>
                    </Form>
                  </Modal>
                </Card.Body>
              </Card>
            </div>
          </div>
        </section>
      </>
      : <>
        <div className="col-lg-4">
          <Card className="container">
            <Card.Body>
              <Card.Title>{brand}</Card.Title>
              <Card.Subtitle className="mb-3">{`${model} | ${color}`}</Card.Subtitle>
              <Card.Text> Price: PHP {price}.00</Card.Text>
              <button type="button" className="btn btn-outline-danger rounded-pill w-100 mb-1" onClick={showModal2}>Add To Cart</button>
              <button type="button" className="btn btn-outline-danger rounded-pill w-100" onClick={showModal}>View Details</button>
              <Modal show={isOpen2} onHide={hideModal2}>
                <Form onSubmit={e => handleAddToCart(e)}>
                  <Modal.Header>
                    <Modal.Title><h4>{brand} ({`${model} | ${color} - PHP ${price}.00`}) </h4></Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form.Group>
                      <Form.Control
                        className="mb-2" 
                        type="text"
                        value={sku} 
                        readOnly/>
                      <Form.Control
                        type="number"
                        value={qty}
                        onChange={e => setQty(e.target.value)} />
                    </Form.Group>  
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="outline-danger rounded-pill" type="submit">Add</Button>
                  </Modal.Footer>
                </Form>
              </Modal>
              <Modal show={isOpen} onHide={hideModal}>
                <Modal.Header>
                  <Modal.Title><h4>{brand} ({`${model} | ${color}`})</h4></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Table striped >
                    <tbody >
                      <tr>
                        <td>Details:</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in congue erat. In hac habitasse platea dictumst. Cras at enim mauris. Nulla rhoncus tempor ultrices.</td>
                      </tr>
                      <tr>
                        <td>Price:</td>
                        <td>PHP {price}.00</td>
                      </tr>
                      <tr>
                        <td>Style:</td>
                        <td>{style}</td>
                      </tr>
                      <tr>
                        <td>SKU:</td>
                        <td>{sku}</td>
                      </tr>
                    </tbody>
                  </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-danger rounded-pill" onClick={hideModal}>Close</Button>
                </Modal.Footer>
              </Modal>
            </Card.Body>
          </Card>
        </div>
      </>*/
	);
};