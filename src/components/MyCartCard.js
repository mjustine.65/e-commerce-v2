// import { useState, useContext } from 'react';
import {Card, Table/*, Modal, Form, Button*/} from 'react-bootstrap';
import Swal from 'sweetalert2';
// import { Navigate } from 'react-router-dom';
// import UserContext from '../UserContext';


export default function MyCartCard({prop, getItemsFn}) {

  const {brand, model, color, style, sku, qty, price, subTotal} = prop;

  const handleDeleteItem = (e) => {
    
    e.preventDefault();

    // fetch(`${process.env.REACT_APP_API_URL}products/del-cart-item`, {
    fetch(`${process.env.REACT_APP_API_URL}products/del-cart-item`, {

      method: 'PUT',
      
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },

      body: JSON.stringify({
        sku: prop.sku,
        isDeleting: true
      })
    })
    .then(res => res.json())
    .then(data => {

      if (data.result === true) {

        getItemsFn()

        Swal.fire({
          title: 'Successfully Deleted!',
          icon: 'success'
        });
        return;
      };
    });
  };

	return (
    <>
      <tr className="text-center">
        <td className="p-3">{prop.brand}</td>
        <td className="p-3">{prop.model}</td>
        <td className="p-3">{prop.style}</td>
        <td className="p-3">{prop.color}</td>
        <td className="p-3">{prop.sku}</td>
        <td className="p-3">PHP {prop.price}.00</td>
        <td className="p-3">{prop.qty}</td>
        <td className="p-3">PHP {prop.subTotal}.00</td>
        <td><button className="btn btn-outline-secondary rounded-pill w-100" onClick={e => handleDeleteItem(e)}>Delete</button></td>
      </tr>
      {/*<section className="container overflow-auto">
        <div className="row justify-content-center">
          <div className="col-lg-10">
            <Card className="container">
              <Card.Body>
                <Card.Title>{brand}</Card.Title>
                <Card.Subtitle className="mb-3">{`${model} | ${color}`}</Card.Subtitle>
                  <Table striped >
                    <tbody >
                      <tr>
                        <td>SKU:</td>
                        <td>{sku}</td>
                      </tr>
                      <tr>
                        <td>Style:</td>
                        <td>{style}</td>
                      </tr>
                      <tr>
                        <td>Price:</td>
                        <td>{`PHP ${price}.00`}</td>
                      </tr>
                      <tr>
                        <td>Qty And Sub Total:</td>
                        <td>{`${qty} | PHP ${subTotal}.00`}</td>
                      </tr>
                    </tbody>
                  </Table>
                  <button className="btn btn-outline-danger rounded-pill" onClick={e => handleDeleteItem(e)}>Delete</button>
              </Card.Body>
            </Card>
          </div>
        </div>
      </section>*/}
    </>
	);
};