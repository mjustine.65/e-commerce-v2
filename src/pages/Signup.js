import { useState, useEffect, useContext } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

// Styles
import { Form, Button, Nav } from 'react-bootstrap';
import '../styles/Login.css';

// Misc Modules
import Swal from 'sweetalert2';

export default function Signup() {

	// getting global state/value of id and isAdmin
	const { idGlobal } = useContext(UserContext);
	const navigate = useNavigate()

	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmedPassword, setConfirmedPassword] = useState('');
	const [displayPassword, setDisplayPassword] = useState(false);
	const [disableBtn, setDisableBtn] = useState(true)

	useEffect(() => {
		(	firstname !== '' &&
			lastname !== '' &&
			email !== '' &&
			password !== '' &&
			confirmedPassword !== '')
		? setDisableBtn(false)
		: setDisableBtn(true)
	}, [firstname, lastname, email, password, confirmedPassword])

	const showPassword = () => {
		setDisplayPassword(!displayPassword);
	};

	const isValidEmail = (email) => {
		const emailPattern = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
		return (emailPattern.test(email))
		? true
		: false;
	};

	const isValidPassword = (password) => {
		const passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
		return (passwordPattern.test(password))
		? true
		: false;
	};

	const isSamePassword = (pw1, pw2) => {
		return (pw1 === pw2)
		? true
		: false;
	}

	const handleSignup = (e) => {
		e.preventDefault();

		if (!isValidEmail(email)) {
			return Swal.fire({
				title: 'Oops! Invalid email address.',
				icon: 'error',
				text: 'Please enter a valid email address.'
			});
		} else if (!isValidPassword(password) ) {
			return Swal.fire({
				title: 'Oops! Invalid password.',
				icon: 'error',
				text: 'Your password must contain at least 8 characters consist of letter, numbers, and symbols.'
			});
		} else if (!isSamePassword(password, confirmedPassword)) {
			return Swal.fire({
				title: 'Oops! Password does not match.',
				icon: 'error',
				text: 'Please try again.'
			});
		} else {
			// fetch(`${process.env.REACT_APP_API_URL}users/signup`, {
			
			fetch(`${process.env.REACT_APP_API_URL}users/signup`, {

				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstname: firstname,
					lastname: lastname,
					email: email,
					password: password,
					confirmedPassword: confirmedPassword
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data.result === false) {
					return Swal.fire({
						title: 'Registration Failed',
						icon: 'error',
						text: 'Email was already registered. Please register with a different email.'
					})
				}

				setFirstname('');
				setLastname('');
				setEmail('');
				setPassword('');
				setConfirmedPassword('');
				navigate("/users/login");
				return Swal.fire({
						title: 'Registration Success',
						icon: 'success',
					})
			})


		}
	};

	return (
		(idGlobal === null)
		? <section className="container mt-5">
			<Form className="row justify-content-center" onSubmit={e => handleSignup(e)}>
				<div className="col-lg-6 p-5 border rounded shadow bg-light" id="form">
					<h1 className="mb-3 text-center">Sign Up</h1>
					<Form.Group>
						<Form.Control 
							className="mb-3"
							type="text"
							value={firstname}
							onChange={e => setFirstname(e.target.value)}
							placeholder="* First name"/>
						<Form.Control 
							className="mb-3"
							type="text"
							value={lastname}
							onChange={e => setLastname(e.target.value)}
							placeholder="* Last name" />
						<Form.Control 
							className="mb-3"
							type="email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							placeholder="* Email" />
						<Form.Control
							className="mb-3" 
							type={(displayPassword) ? "text" : "password"}
							value={password}
							onChange={e => setPassword(e.target.value)} 
							placeholder="* Password" />
						<Form.Control
							className="mb-2" 
							type={(displayPassword) ? "text" : "password"}
							value={confirmedPassword}
							onChange={e => setConfirmedPassword(e.target.value)} 
							placeholder="* Confirm Password" />
					</Form.Group>
					<Form.Group className="mb-5">
						<input className="form-check-input" type="checkbox" onClick={showPassword} id="showpw"/>
						<label htmlFor="showpw" className="mx-2">Show password</label>
					</Form.Group>
					{
						(disableBtn)
						? <Button variant="outline-danger rounded-pill" className="w-100 mb-5" disabled>Register</Button>
						: <Button variant="outline-primary rounded-pill" className="w-100 mb-5" type="submit">Register</Button>
					}
					<Nav.Link className="text-center" as={ Link } to="/users/login">Already have an account?</Nav.Link>
				</div>
			</Form>
		</section>
		: <Navigate to="/" />
	);
};