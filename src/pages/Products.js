// libraries
import { useState, useEffect, useContext } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';

// components
import ProductCard from '../components/ProductCard';
import ProductTable from '../components/ProductTable';

// context
import UserContext from '../UserContext';

// misc libraries
import Swal from 'sweetalert2';

export default function AdminProducts() {

    // getting global state/value of id and isAdmin
    const { idGlobal, isAdminGlobal } = useContext(UserContext);

    // init states/values
    const [products, setProducts] = useState([]);
    const [brand, setBrand] = useState('');
    const [style, setStyle] = useState('');
    const [model, setModel] = useState('');
    const [size, setSize] = useState('');
    const [color, setColor] = useState('');
    const [sku, setSku] = useState('');
    const [price, setPrice] = useState('');
    const [stocks, setStocks] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const [disableBtn, setDisableBtn] = useState(true);
    

    useEffect(() => {
        (brand !== '' &&
            style !== '' &&
            model !== '' &&
            // size !== '' &&
            color !== '' &&
            sku !== '' &&
            price !== '' &&
            stocks !== '') ?
        setDisableBtn(false): setDisableBtn(true)
    }, [brand, style, model, size, color, sku, price, stocks])

    

    
    const showModal = () => {

        setIsOpen(prevValue => true);
        return;
    };

    const hideModal = () => {
        setBrand('');
        setStyle('');
        setModel('');
        setSize('');
        setColor('');
        setSku('');
        setPrice('');
        setStocks('');
        setIsOpen(false);
    };

    const handleAddProduct = (e) => {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}products/add`, {

                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    brand: brand,
                    style: style,
                    model: model,
                    size: size,
                    color: color,
                    sku: sku,
                    price: price,
                    stocks: stocks,
                })
            })
            .then(res => res.json())
            .then(data => {

                if (data.result === false) {

                    return Swal.fire({
                        title: 'Oops!',
                        icon: 'info',
                        text: `This product with ${sku} was already added in the system.`
                    });
                };

                setBrand('');
                setStyle('');
                setModel('');
                setSize('');
                setColor('');
                setSku('');
                setPrice('');
                setStocks('');

                hideModal();

                getAllProds();

                Swal.fire({
                    title: 'Successfully Added Product',
                    icon: 'success',
                });
                return; 
            });

    };

    const getAvailProds = () => {

        fetch(`${process.env.REACT_APP_API_URL}products/available`, {

            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            setProducts(prevValue => data.map(product => {
                
                return (
                    <ProductCard key={product._id} prop={product} />
                )
            }));
            return; 
        })
        .catch(error => console.log(error))
        return
    }

    const getAllProds = () => {

        fetch(`${process.env.REACT_APP_API_URL}products/all`, {
        // fetch(`http://localhost:4000/products/view/all`, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            setProducts(prevValue => data.map(product => {
                
                return (
                    <ProductTable key={product._id} prop={product} getAllProdsFn={getAllProds}/>
                )
            }));
            return;
        })
        .catch(error => console.log(error))
        return
    }

    // side effect to be applied after rendering components
    useEffect(() => {

        // conditional state for what products to display
            if (isAdminGlobal) {
                // fetch(`${process.env.REACT_APP_API_URL}products/view/all`, {
                getAllProds()
            } else {
                // fetch(`${process.env.REACT_APP_API_URL}products/view/all`, {
                getAvailProds()
            }
        

    }, [idGlobal, isAdminGlobal])

    return (
        // conditional rendering
        (isAdminGlobal) ?
        <div className="container-fluid">
            <div className="row">
                <main className="col p-5 bg-light overflow-auto" style={{maxHeight: "89vh" }}>
                    <div className="row justify-content-center">
                        {/*{products}*/}
                        <table className="table table-striped table-bordered">
                            <thead>
                                <tr className="text-center">
                                    <th>Product ID</th>
                                    <th>Brand</th>
                                    <th>Model</th>
                                    <th>Style</th>
                                    <th>Color</th>
                                    <th>SKU</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Status</th>
                                    <th>
                                    <button type="button" className="btn btn-danger rounded-pill w-100" onClick={showModal}>Add Product</button>
                                        {/*{
                                            (items.length === 0)
                                            ? <button type="button" className="btn btn-danger rounded-pill w-100" disabled>Submit Order</button>
                                            : <button type="button" className="btn btn-danger rounded-pill w-100" onClick={e => handleSubmitOrder(e)}>Submit Order</button>
                                        }*/}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {products}
                            </tbody>
                        </table>
                    </div>      
                </main>
            </div>

            {/* MODAL FOR ADDING PRODUCT */}
            <Modal show={isOpen} onHide={hideModal}>
                <Form onSubmit={e => handleAddProduct(e)}>
                <Modal.Header>
                    <Modal.Title><h4>Add Product</h4></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group>
                        <Form.Control 
                            className="mb-2"
                            type="text"
                            value={brand}
                            onChange={e => setBrand(e.target.value)}
                            placeholder="* Brand (e.g. SMK, AVG, LS2)"/>
                        <Form.Select aria-label="Default select example" className="mb-2" value={style} onChange={e => setStyle(e.target.value)}>
                            <option>Select Style</option>
                            <option value="Full-face">Full Face</option>
                            <option value="Half-face">Half Face</option>
                            <option value="Modular">Modular</option>
                            <option value="Off road">Off Road</option>
                        </Form.Select>
                        <Form.Control 
                            className="mb-2"
                            type="text"
                            value={model}
                            onChange={e => setModel(e.target.value)}
                            placeholder="* Model Name" />
                        {/*<Form.Select aria-label="Default select example" className="mb-2" value={size} onChange={e => setSize(e.target.value)}>
                            <option>Select Size</option>
                            <option value="XS">Extra Small</option>
                            <option value="SM">Small</option>
                            <option value="MD">Medium</option>
                            <option value="LG">Large</option>
                            <option value="XL">Extra Large</option>
                        </Form.Select>*/}
                        <Form.Control 
                            className="mb-2"
                            type="text"
                            value={color}
                            onChange={e => setColor(e.target.value)}
                            placeholder="* Color" />
                        <Form.Control 
                            className="mb-2"
                            type="text"
                            value={sku}
                            onChange={e => setSku(e.target.value)}
                            placeholder="* SKU (e.g. EXHE-STEXS000)" />
                        <Form.Control 
                            className="mb-2"
                            type="text"
                            value={price}
                            onChange={e => setPrice(e.target.value)}
                            placeholder="* Price" />
                        <Form.Control 
                            className="mb-2"
                            type="text"
                            value={stocks}
                            onChange={e => setStocks(e.target.value)}
                            placeholder="* Stocks" />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                {
                    (disableBtn)
                    ? <Button variant="outline-danger rounded-pill" disabled>Add</Button>
                    : <Button variant="outline-danger rounded-pill" type="submit">Add</Button>
                }
                    <Button variant="outline-danger rounded-pill" onClick={hideModal}>Cancel</Button>
                </Modal.Footer>
                </Form>
            </Modal>
        </div>
        
        :
        <div className="container-fluid">
            <div className="row">
                <main className="col p-5 bg-secondary overflow-auto" style={{maxHeight: "89vh" }}>
                    <div className="row justify-content-center">
                        {products}
                    </div>      
                </main>
            </div>
        </div>

        // ( isAdminGlobal === null || isAdminGlobal === false)
        // ? 
        // : (user.isAdmin)
        // ? <div className="container-fluid">
        //     <div className="row">
        //         <aside className="col-lg-3 p-5 bg-dark">
        //             <button type="button" className="btn btn-outline-danger rounded-pill w-100 mb-2" onClick={showModal}>Add Product</button>
        //             <Modal show={isOpen} onHide={hideModal}>
        //                 <Form onSubmit={e => handleAddProduct(e)}>
        //                 <Modal.Header>
        //                     <Modal.Title><h4>Add Product</h4></Modal.Title>
        //                 </Modal.Header>
        //                 <Modal.Body>
        //                         <Form.Group>
        //                             <Form.Control 
        //                                 className="mb-2"
        //                                 type="text"
        //                                 value={brand}
        //                                 onChange={e => setBrand(e.target.value)}
        //                                 placeholder="* Brand (e.g. SMK, AVG, LS2)"/>
        //                             <Form.Select aria-label="Default select example" className="mb-2" value={style} onChange={e => setStyle(e.target.value)}>
        //                                 <option>Select Style</option>
        //                                 <option value="Full-face">Full Face</option>
        //                                 <option value="Half-face">Half Face</option>
        //                                 <option value="Modular">Modular</option>
        //                                 <option value="Off road">Off Road</option>
        //                             </Form.Select>
        //                             <Form.Control 
        //                                 className="mb-2"
        //                                 type="text"
        //                                 value={model}
        //                                 onChange={e => setModel(e.target.value)}
        //                                 placeholder="* Model Name" />
        //                             <Form.Select aria-label="Default select example" className="mb-2" value={size} onChange={e => setSize(e.target.value)}>
        //                                 <option>Select Size</option>
        //                                 <option value="XS">Extra Small</option>
        //                                 <option value="SM">Small</option>
        //                                 <option value="MD">Medium</option>
        //                                 <option value="LG">Large</option>
        //                                 <option value="XL">Extra Large</option>
        //                             </Form.Select>
        //                             <Form.Control 
        //                                 className="mb-2"
        //                                 type="text"
        //                                 value={color}
        //                                 onChange={e => setColor(e.target.value)}
        //                                 placeholder="* Color" />
        //                             <Form.Control 
        //                                 className="mb-2"
        //                                 type="text"
        //                                 value={sku}
        //                                 onChange={e => setSku(e.target.value)}
        //                                 placeholder="* SKU (e.g. EXHE-STEXS000)" />
        //                             <Form.Control 
        //                                 className="mb-2"
        //                                 type="text"
        //                                 value={price}
        //                                 onChange={e => setPrice(e.target.value)}
        //                                 placeholder="* Price" />
        //                             <Form.Control 
        //                                 className="mb-2"
        //                                 type="text"
        //                                 value={stocks}
        //                                 onChange={e => setStocks(e.target.value)}
        //                                 placeholder="* Stocks" />
        //                         </Form.Group>
        //                 </Modal.Body>
        //                 <Modal.Footer>
        //                     {
        //                         (disableBtn)
        //                         ? <Button variant="outline-danger rounded-pill" disabled>Add</Button>
        //                         : <Button variant="outline-danger rounded-pill" type="submit">Add</Button>
        //                     }
        //                     <Button variant="outline-danger rounded-pill" onClick={hideModal}>Cancel</Button>
        //                 </Modal.Footer>
        //                 </Form>
        //             </Modal>
        //         </aside>
        //         <main className="col p-5 bg-secondary overflow-auto" style={{maxHeight: "89vh" }}>
        //             {products}
        //         </main>
        //     </div>
        // </div> 
        // : <div className="container-fluid">
        //     <div className="row">
        //         <main className="col p-5 bg-secondary overflow-auto" style={{maxHeight: "89vh" }}>
        //             <div className="row justify-content-center">
        //                 {products}
        //             </div>      
        //         </main>
        //     </div>
        // </div> 

    );
};