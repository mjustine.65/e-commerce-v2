import { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

// Styles
import { Button } from 'react-bootstrap';
import '../App.css';

export default function Login() {
	const { user } = useContext(UserContext);
	return (
		<section className="container mt-5">
			<div className="row justify-content-center">
				<div className="col-lg-6 p-5 border rounded shadow bg-light">
					<h1 className="mb-3">Page 404</h1>
					<p>Oops! Sorry but we can't seem to find the page you are looking for.</p>
					<Button variant="outline-secondary rounded-pill" className="w-100" as={ Link } to={(user.isAdmin) ? "/admins" : "/"}>Back to Home</Button>
				</div>
			</div>
		</section>
	);
};